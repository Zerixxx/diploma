/**
 * Created by 1 on 3/17/2016.
 */

(function(){
    "use strict";

    function foo(str, sep){
        let open = [];

        for(let ch of str){
            for(let i = 0; i < sep.length; i++){
                if(ch === sep[i]){
                    if(i%2 === 0 && sep[i] !== open[open.length - 1]) open.push(ch);
                    else if(sep[i-1] === open[open.length - 1]) open.pop();
                    else return false;
                }
            }
        }

        return open.length === 0;
    }


    let s = foo("Sensei says [-yes[-]]!", "--");
    console.log(s);

})();

