/**
 * Created by Pasha's owner on 17.04.16.
 */



(function(app){
    "use strict";

    app.factory('profileService', ['$http', function($http){
        return {
            loadComments: function(profileId){
                return $http.get('/profile/comments/' + profileId);
            },

            saveComment: function(comment){
                return $http.post('/profile/comments/add', {comment: comment});
            },

            currentUserInfo: function () {
                return $http.get('/profile/user-info');
            },

            loadPlacesOnMap: function () {
                return $http.get('/profile/places-on-map');
            }
        };
    }]);

    app.controller('profileController', ['$scope','$timeout', '$interval','$filter','MapService','profileService', function($scope,$timeout,$interval,$filter,map, profileService){
        var profileId = parseInt(document.getElementById('userId').innerHTML);
        var markers = [];

        function formatCommentsTime(comments){
            for(var i = 0; i < comments.length; i++){
                comments[i].creationdate = $filter('date')(comments[i].creationdate, "yyyy-MM-dd");
            }
            return comments;
        }

        function loadComments(){
            profileService.loadComments(profileId).then(function (response) {
                $scope.comments = formatCommentsTime(response.data);
            },function(err){
                console.log(err);
            });
        }

        function updatePlacesOnMap(places){
            var m, coords;
            for(var i = 0; i < places.length; i++){
                coords = new google.maps.LatLng(parseFloat(places[i].latitude),parseFloat(places[i].longitude));
                m = map.createMarker(coords);
                if(m != null){
                    map.initInfoWindow(m, places[i]);
                    markers.push(m);
                }
            }
        }


        profileService.currentUserInfo().then(function (response) {
            $scope.user = response.data;
        },function(err){
            console.log(err);
        });

        profileService.loadPlacesOnMap().then(function (response) {
            $scope.placesOnMap = response.data;
        },function(err){
            console.log(err);
        });

        $scope.saveComment = function(form){
            if(!form.$invalid){
                var comment = {
                    commentText: $scope.commentText,
                    creationDate: $filter('date')(new Date(), "yyyy-MM-dd"),
                    ownerId: $scope.user.id,
                    receiverId: profileId
                };

                profileService.saveComment(comment).then(function (response) {
                    loadComments();
                },function(err){
                    console.log(err);
                });
            }
        };

        $timeout(function() {
            map.init();
            updatePlacesOnMap($scope.placesOnMap);
        }, 3000);

        loadComments();

        $interval(loadComments, 3000);

    }]);



})(angular.module('App'));

