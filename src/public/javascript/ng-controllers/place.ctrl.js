/**
 * Created by owner on 24.04.16.
 */

(function(app){
    "use strict";
    
    app.factory('PlaceService', ['$http', function($http){

        var save = function(model){
            return $http.post('/places/save', model);
        };

        var getAllPlaceTypes = function(){
            return $http.get('/places/types');
        };

        var searchForPlaces = function(searchModel){
            return $http.post('/places/search', searchModel);
        };

        var uploadImage = function(image){
            return $http.post('/images/upload/avatar', {image: image}, {
                headers: {'Content-Type': 'image/jpeg' }
            });
        };

        var getUserRatingForPlace = function(placeId){
            var url = '/places/' + placeId + '/avg-rating';
            return $http.get(url);
        };

        var savePlaceRating = function (value, placeId) {
            return $http.post('/places/rating/save', {rating: value, placeId: placeId});
        };
        
        var getUserInfo = function(){
            return $http.get('/profile/user-info');
        };

        var loadComments = function(placeId){
            return $http.get('/places/comments/' + placeId);
        };

        var saveComment = function(comment){
            return $http.post('/places/comments/add', {comment: comment});
        };

        var saveToPersonalMap = function(placeId){
            return $http.post('/places/save-to-map', {placeId: placeId});
        };

        var saveToRoute = function(placeId){
            return $http.post('/places/save-to-route', {placeId: placeId});
        };

        return {
            save: save,
            getAllPlaceTypes: getAllPlaceTypes,
            searchForPlaces: searchForPlaces,
            uploadImage: uploadImage,
            getUserRatingForPlace: getUserRatingForPlace,
            savePlaceRating: savePlaceRating,
            getUserInfo: getUserInfo,
            loadComments: loadComments,
            saveComment: saveComment,
            saveToPersonalMap: saveToPersonalMap,
            saveToRoute: saveToRoute
        }
    }]);

    app.controller('searchPlaceController', ['$scope', 'MapService', 'PlaceService', 'Utils', function($scope, map, placeService, utils){
        $scope.places = [];
        $scope.searchModel = {
            address: '',
            typeId: '',
            name: ''
        };
        var markers = [];

        map.init();
        map.setZoom(5);

        placeService.getUserInfo().then(function(response){
            $scope.user = response.data;
        },function (err) {
            location.href = '/account/login';
        });

        function updatePlacesOnMap(places){
            var m, coords;
            for(var i = 0; i < places.length; i++){
                coords = new google.maps.LatLng(parseFloat(places[i].latitude),parseFloat(places[i].longitude));
                m = map.createMarker(coords);
                if(m != null){
                    map.initInfoWindow(m, places[i]);
                    markers.push(m);
                }
            }
        }

        function clearMap(){
            for(var i = 0; i < markers.length; i++){
                map.deleteMarker(markers[i]);
            }
        }

        // get all types for select control
        placeService.getAllPlaceTypes().then(function(response){
            $scope.placeTypes = response.data;
        }, function(error){
            // TODO
        });

        $scope.setCircleArea = function(){
            function setCircle(coords){
                var lngLtd = {
                    latitude: coords.lat().toString(),
                    longitude: coords.lng().toString()
                };

                map.createCircle();
            }
        };

        $scope.search = function(){
            console.log($scope.searchModel);
            map.getAddressComponents($scope.searchModel.address, function(formatted){
                var model = {
                    address: {
                        country: formatted.country,
                        region: formatted.region,
                        city: formatted.city
                    },
                    type: $scope.searchModel.typeId
                        ? $scope.placeTypes[parseInt($scope.searchModel.typeId)-1].name
                        : '',
                    name: $scope.searchModel.name
                };

                placeService.searchForPlaces(model).then(function(response){
                    console.log(response.data);

                    for(var i = 0; i < response.data.length; i++){
                        response.data[i].avgrating = parseInt(response.data[i].avgrating);
                    }
                    $scope.places = response.data;
                    console.log($scope.places);
                    clearMap();
                    updatePlacesOnMap($scope.places);
                }, function(error){
                    // TODO
                });

            });
        };

        $scope.search();
    }]);

    app.controller('placePageController',['$scope', 'PlaceService', '$filter', '$interval', function($scope, placeService, $filter, $interval){
        var placeId = parseInt(document.getElementById('placeId').innerHTML);

        function formatCommentsTime(comments){
            for(var i = 0; i < comments.length; i++){
                comments[i].creationdate = $filter('date')(comments[i].creationdate, "yyyy-MM-dd");
            }
            return comments;
        }

        function loadComments(){
            placeService.loadComments(placeId).then(function (response) {
                $scope.comments = formatCommentsTime(response.data);
            },function(err){
                console.log(err);
            });
        }

        placeService.getUserInfo().then(function (response) {
            $scope.user = response.data;
        },function(err){
            console.log(err);
        });

        placeService.getUserRatingForPlace(placeId).then(function(response){
            $scope.userRating = response.data;
        }, function (err) {
            console.log(err);
        });

        $scope.saveRating = function(param){
            console.log(param);
            if($scope.userRating == param)  return;
            else{
                placeService.savePlaceRating(param, placeId).then(function(response){
                    $scope.userRating = param;
                },function(err){
                    console.log(err);
                })
            }
        };

        $scope.saveComment = function(form){
            if(!form.$invalid){
                var comment = {
                    commentText: $scope.commentText,
                    creationDate: $filter('date')(new Date(), "yyyy-MM-dd"),
                    ownerId: $scope.user.id,
                    placeId: placeId
                };

                placeService.saveComment(comment).then(function (response) {
                    loadComments();
                },function(err){
                    console.log(err);
                });
            }
        };

        $scope.saveToPersonalMap = function () {
            placeService.saveToPersonalMap(placeId).then(function (response) {
                $('#saveToMapIcon').removeClass().addClass('glyphicon glyphicon-ok');
            },function(err){
                console.log(err);
            });
        };

        $scope.saveToRoute = function () {
            placeService.saveToRoute(placeId).then(function (response) {
                $('#saveToRouteIcon').removeClass().addClass('glyphicon glyphicon-ok');
            },function(err){
                console.log(err);
            });
        };

        loadComments();
    }]);

    app.controller('createPlaceController', ['$scope','$anchorScroll', '$location','$filter', 'PlaceService', 'MapService','Utils','Upload',
        function($scope, $anchorScroll, $location,$filter, placeService, map, utils, Upload){
            $scope.placeTypes = [];
            var images = [];
            var addressMarker;
            $scope.place = {
                name: null,
                startDate: new Date(),
                endDate: new Date(),
                openingTime: new Date(1970, 0, 1, 9, 0, 0),
                closingTime: new Date(1970, 0, 1, 18, 0, 0),
                minPrice: "0",
                maxPrice: "0",
                street: null,
                building: null,
                latitude: '333',
                longitude: '333',
                phone: '+320111111111',
                email: 'admin@mail.com',
                shortDescription: 'sdddddddddddddddddddddddddddd',
                fullDescription: 'sdddddddddddddddddddddddddddd',
                avatarImageId: null,
                addressId: null,
                typeId: 1,
                ownerId: null
            };

            $scope.address = {
                country: 'Ukraine',
                region: 'Kyiv',
                city: 'Kyiv'
            };

            map.init();

            // get all types for select control
            placeService.getAllPlaceTypes().then(function(response){
                $scope.placeTypes = response.data;
            }, function(error){
                // TODO
            });

            // anchor routing
            $scope.gotoSection = function(name){
                $location.hash(name);
                $anchorScroll();
            };

            addressMarker = map.createMarker(new google.maps.LatLng(50.450100, 30.523400), "Kyiv");

            // actions
            $scope.setMarker = function(){
                var address = angular.extend({},$scope.address, {street: $scope.place.street, building: $scope.place.building });
                //console.log(address);
                address = utils.convertAddressObjToString(address);
                function setMarker(coords){
                    $scope.place.latitude = coords.lat().toString();
                    $scope.place.longitude = coords.lng().toString();
                    map.deleteMarker(addressMarker);
                    addressMarker = map.createMarker(coords, "Here!");
                    map.setCenter(addressMarker.position);
                    map.setZoom(16);
                }
                map.findPlaceByAddress(address,setMarker);
            };

            $scope.savePlace = function(form){
                if(!form.$invalid){
                    // convert to db format

                    $scope.place.startDate = $filter('date')($scope.place.startDate, "yyyy-MM-dd");
                    $scope.place.endDate = $filter('date')($scope.place.endDate, "yyyy-MM-dd");
                    $scope.place.openingTime = $filter('date')($scope.place.openingTime, "HH:mm:ss");
                    $scope.place.closingTime = $filter('date')($scope.place.closingTime, "HH:mm:ss");


                    Upload.upload({
                        url: '/places/save',
                        headers : {
                            'Content-Type': 'multipart/form-data'
                        },
                        arrayKey: '',
                        data: {
                            place: $scope.place,
                            address: $scope.address,
                            images: images
                        }
                    }).then(function (response) {
                        images = [];
                        location.href = '/places/' + response.data.id;
                    }, function (resp) {
                        console.log('Error status: ' + resp.status);
                    });
                }else{
                    console.log(form);
                    console.log('invalid create place form');
                }
            };

            $scope.uploadFiles = function(files, errFiles) {
                images = files;
                console.log(images);
            };
    }]);

    return app;

})(angular.module('App'));