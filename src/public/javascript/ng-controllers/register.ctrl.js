/**
 * Created by owner on 23.02.16.
 */

(function(app){
    "use strict";

    app.factory('registerService', ['$http', function($http){
        return {
            register: function(registrationModel){
                return $http.post('/account/register', {registrationModel: registrationModel});
            }
        };
    }]);

    app.controller('registrationController', ['$scope', 'registerService', function($scope, registerService){
        $scope.registrationModel = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            isBlocked: false,
            addressId: null
        };

        $scope.register = function(form){
            if(!form.$invalid){
                registerService.register($scope.registrationModel).then(function(response) {
                    location.href = response.data.returnUrl;
                },function(error){
                    $scope.$emit('BadResponseEvent', error.msg);
                });
            }
        };
    }]);

    return app;

})(angular.module('App'));
