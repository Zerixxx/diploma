/**
 * Created by owner on 23.02.16.
 */

(function(app){
    "use strict";

    app.factory('loginService', ['$http', function($http){
        return {
            login: function(loginModel){
                return $http.post('/account/login', {loginModel: loginModel});
            }
        };
    }]);

    app.controller('loginController', ['$scope', 'loginService', function($scope, loginService){
        $scope.loginModel = {
            email: "",
            password: ""
        };

        $scope.login = function(form){
            if(!form.$invalid){
                loginService.login($scope.loginModel).then(function(response){
                    location.href = response.data.redirectUrl;
                }, function(error){
                    $scope.$emit('BadResponseEvent', error.msg);
                });
            }
        };

    }]);

    return app;

})(angular.module('App'));