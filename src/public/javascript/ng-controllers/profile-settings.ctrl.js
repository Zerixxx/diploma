/**
 * Created by 1 on 3/25/2016.
 */

(function(app){
    "use strict";

    app.factory('settingsService', ['$http', function($http){
        function save(settings){
            return $http.post('/profile/settings/save', settings);
        }

        function getUserInfo(){
            return $http.get('/profile/user-info');
        }

        return {
            save: save,
            getUserInfo:  getUserInfo
        };
    }]);

    app.controller('profileSettingsController', ['$scope', 'Utils','settingsService', 'Upload', function($scope, utils, settingsService, Upload){

        $scope.user = {};
        var images = [];

        settingsService.getUserInfo().then(function(response){
            $scope.user = response.data;
        }, function(error){
            // TODO
        });

        $scope.save = function(form){
            if(!form.$invalid){

                Upload.upload({
                    url: '/profile/settings',
                    headers : {
                        'Content-Type': 'multipart/form-data'
                    },
                    arrayKey: '',
                    data: {
                        user: $scope.user,
                        images: images
                    }
                }).then(function (response) {
                    images = [];
                    location.href = '/profile/' + $scope.user.id;
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                });
            }else{
                console.log(form);
                console.log('invalid user settings form');
            }
        };

        $scope.uploadAvatar = function(files, errFiles) {
            images = files;
            console.log(images);
        };
    }]);

})(angular.module('App'));
