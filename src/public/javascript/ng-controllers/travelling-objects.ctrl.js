/**
 * Created by 1 on 4/12/2016.
 */

(function(app){
    "use strict";

    app.controller('travellingObjectsController', ['$scope', function($scope){

        $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };

    }]);

})(angular.module("App"));
