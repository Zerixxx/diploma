/**
 * Created by Owner on 6/15/2016.
 */

(function(app){
    "use strict";
    
    app.factory('routeService', function($http){
        
        return {
            getPlaces: function(){
                return $http.get('/routes/')
            },

            removePlaceFromRoute: function(placeId, routeId){
                return $http.post('/routes/remove-place', {placeId: placeId, routeId: routeId});
            }
        };
    });

    app.controller('routeController', ['$scope', function($scope){
        $scope.places = [1,2,3,4,5];
        var routeId = parseInt(document.getElementById('routeId').innerHTML);

        $scope.removePlaceFromRoute = function(place){

        };
    }]);
    
})(angular.module('App'));
