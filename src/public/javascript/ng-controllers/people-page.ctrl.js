/**
 * Created by Owner on 6/16/2016.
 */

(function(app){
    "use strict";

    app.factory('peopleService', ['$http', function($http){
        return {
            searchForPeople: function(model){
                var url = '/people/search?profileId=' + model.profileId + '&userName=' + model.userName;
                return $http.get(url);
            }
        }
    }]);

    app.controller('peoplePageController', ['$scope', 'peopleService', function($scope, peopleService){
        $scope.searchModel = {
            profileId: '',
            userName: ''
        };

        $scope.searchUserResult = [];

        $scope.friends = [];

        $scope.searchUsers = function(form){
            if($scope.searchModel.profileId.length ||
                $scope.searchModel.userName.length){
                peopleService.searchForPeople($scope.searchModel).then(function(response){
                    $scope.searchUserResult = response.data;
                }, function (err) {
                    console.log(err);
                });
            }
        };

        $scope.addToFriends = function(user){
            var found = false;
            for(var i = 0; i < $scope.friends.length; i++) {
                if ($scope.friends[i].id == user.id) {
                    found = true;
                    break;
                }
            }
            if(!found){
                $scope.friends.push(user);
            }
        };

        $scope.removeFromFriends = function(user){
            for(var i = 0; i < $scope.friends.length; i++) {
                if ($scope.friends[i].id == user.id) {
                    $scope.friends.splice(i,1);
                    break;
                }
            }
        };


    }]);

    return app;

})(angular.module('App'));