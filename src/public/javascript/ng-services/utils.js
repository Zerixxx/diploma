/**
 * Created by 1 on 4/4/2016.
 */

var utils = (function(){
    "use strict";

    function convertAddressObjToString(address){
        if(typeof address != 'object'){
            return null;
        }

        return  [address.country, address.region, address.city, address.street, address.building]
            .filter(function (val) {
                return val;
            }).join(', ');
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    function buildInfoWindowContent(place){
        return  '<div id="iw-container">' +
            '<div class="iw-title"><a href="/places/'+place.id+'">'+place.name+'</a></div>' +
            '<div class="iw-content">' +
            '<p id="infoWindow-'+place.id+'" hidden>place.id</p>' +
            '<div class="iw-subTitle">Short Description</div>' +
            '<p>'+place.shortdescription+'</p>' +
            '<div class="iw-subTitle">Contacts</div>' +
            'Phone: '+place.phone+'<br>E-mail: '+place.email+'</p>'+
            '</div>' +
            '<div class="iw-bottom-gradient"></div>' +
            '</div>';
    }

    return {
        convertAddressObjToString: convertAddressObjToString,
        guid: guid,
        buildInfoWindowContent: buildInfoWindowContent
    }
})();
