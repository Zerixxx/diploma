/**
 * Created by owner on 19.05.16.
 */


var myMapService = (function(utils){
    "use strict";

    var map;
    var geocoder;

    var init = function(myOptions){
        geocoder = new google.maps.Geocoder();

        var myOptions = myOptions || {
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var initialLocation;
        var siberia = new google.maps.LatLng(60, 105);
        var newyork = new google.maps.LatLng(40.69847032728747, -73.9514422416687);
        var browserSupportFlag = Boolean();
        var elementId = myOptions.id || "map";
        map = new google.maps.Map(document.getElementById(elementId), myOptions);

        // Try W3C Geolocation (Preferred)
        if(navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {
                initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                map.setCenter(initialLocation);
            }, function() {
                handleNoGeolocation(browserSupportFlag);
            });
        }
        // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }

        function handleNoGeolocation(errorFlag) {
            if (errorFlag == true) {
                console.log("Geolocation service failed.");
                initialLocation = newyork;
            } else {
                console.log("Your browser doesn't support geolocation. We've placed you in Siberia.");
                initialLocation = siberia;
            }
            map.setCenter(initialLocation);
        }
    };

    var findPlaceByAddress = function(address, cb){
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                cb(results[0].geometry.location);
            } else {
                console.log("Geocode was not successful for the following reason: " + status);
            }
        });
    };

    var findPlaceByCoords = function (latlng, cb) {
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    cb(results[1].formatted_address);
                } else {
                    console.log('No results found');
                }

            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    };

    var getAddressComponents = function(address ,cb){
        geocoder.geocode( { 'address': address}, function(results, status) {
            var res = {city: '', region: '', country: '', street: '', building: '', formattedAddress: '', latitude: '', longitude: ''};
            if (status == google.maps.GeocoderStatus.OK) {
                for(var i = 0; i < results[0].address_components.length; i++){
                    if (results[0].address_components[i].types[0] == "locality") {
                        res.city = results[0].address_components[i].long_name; continue;
                    }
                    if (results[0].address_components[i].types[0] == "administrative_area_level_1") {
                        res.region = results[0].address_components[i].long_name; continue;
                    }
                    if (results[0].address_components[i].types[0] == "country") {
                        res.country = results[0].address_components[i].long_name; continue;
                    }
                    if (results[0].address_components[i].types[0] == "route") {
                        res.street = results[0].address_components[i].long_name; continue;
                    }
                    if (results[0].address_components[i].types[0] == "street_number") {
                        res.building = results[0].address_components[i].long_name;
                    }
                }
                res.formattedAddress = results[0].formatted_address;
                res.latitude = results[0].geometry.location.lat;
                res.longitude = results[0].geometry.location.lng;
            }
            cb(res);
        });
    };

    var createMarker = function (coords, title) {
        if(coords != null){
            var _options = {
                map: map,
                position: coords,
                title: title
            };
            return new google.maps.Marker(_options);
        }
        return null;
    };

    var deleteMarker = function(marker){
        if(marker != null)  marker.setMap(null);
    };

    var setCenter = function(position){
        map.setCenter(position);
    };

    var setZoom = function(zoom){
        map.setZoom(zoom);
    };

    var createCircle = function(lngLtd, radius){
        var opt = {
            strokeColor: '#4FBA6F',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#4FBA6F',
            fillOpacity: 0.25,
            map: map,
            center: lngLtd,
            radius: radius
        };

        if(coords != null || radius != null){
            return new new google.maps.Circle(opt);
        }
        return null;
    };

    var initInfoWindow = function(marker, place){
        var content = utils.buildInfoWindowContent(place);
        var infowindow = new google.maps.InfoWindow({
            content: content,
            maxWidth: 350
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });

        google.maps.event.addListener(infowindow, 'domready', function() {

            // Reference to the DIV that wraps the bottom of infowindow
            var iwOuter = $('.gm-style-iw');

            /* Since this div is in a position prior to .gm-div style-iw.
             * We use jQuery and create a iwBackground variable,
             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
             */
            var iwBackground = iwOuter.prev();

            // Removes background shadow DIV
            iwBackground.children(':nth-child(2)').css({'display' : 'none'});

            // Removes white background DIV
            iwBackground.children(':nth-child(4)').css({'display' : 'none'});

            // Moves the infowindow 115px to the right.
            iwOuter.parent().parent().css({left: '115px'});

            // Moves the shadow of the arrow 76px to the left margin.
            iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

            // Moves the arrow 76px to the left margin.
            iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

            // Changes the desired tail shadow color.
            iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

            // Reference to the div that groups the close button elements.
            var iwCloseBtn = iwOuter.next();

            // Apply the desired effect to the close button
            iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

            // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
            if($('.iw-content').height() < 140){
                $('.iw-bottom-gradient').css({display: 'none'});
            }

            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
            iwCloseBtn.mouseout(function(){
                $(this).css({opacity: '1'});
            });
        });
    };

    return {
        init: init,
        findPlaceByAddress: findPlaceByAddress,
        findPlaceByCoords: findPlaceByCoords,
        createMarker: createMarker,
        deleteMarker: deleteMarker,
        setCenter: setCenter,
        setZoom: setZoom,
        createCircle: createCircle,
        getAddressComponents: getAddressComponents,
        initInfoWindow: initInfoWindow
    }
})(utils);
