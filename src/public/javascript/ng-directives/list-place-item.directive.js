/**
 * Created by Owner on 6/12/2016.
 */


(function(app){
    "use strict";

    app.directive('listPlaceItem', function($http){
        return {
            restrict: 'E',
            scope: {
                place: '=place',
                user: '=user'
            },
            templateUrl: '/javascript/ng-templates/list-place-item.template.html',
            controller: function ($scope, $element, $attrs) {
                $scope.avgRating = parseInt($scope.place.avgrating / 2);
                $scope.isInRoute = false;
                $scope.onRouteActionChanged = function(){
                    if(!$scope.isInRoute){
                        $http.post('/routes/add-place', {placeId: $scope.place.id, userId: $scope.user.id}).then(function(){
                            $scope.isInRoute = !$scope.isInRoute;
                        }, function(err){
                            // TODO
                        });
                    }else{
                        $http.post('/routes/remove-place', {placeId: $scope.place.id, userId: $scope.user.id}).then(function(){
                            $scope.isInRoute = !$scope.isInRoute;
                        }, function(err){
                            // TODO
                        });
                    }
                };
            }
        };
    });

    return app;

})(angular.module('App'));
