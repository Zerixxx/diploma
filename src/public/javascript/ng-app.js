/**
 * Created by owner on 23.02.16.
 */

(function(utils, myMapService){
    "use strict";
    return angular.module('App', ['ngMessages', 'ngAnimate', 'ngRoute', 'ngFileUpload'])
        .controller('BaseController', ['$scope', function($scope){

            $scope.errorMsg = '';

            $scope.$on('BadRequestEvent', function(e, msg){
                if(typeof msg === 'string')
                    $scope.errorMsg = msg;
            });

        // inject non angular services
        }]).value('Utils', utils)
        .value('MapService', myMapService);
})(utils, myMapService);