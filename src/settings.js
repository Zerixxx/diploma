/**
 * Created by 1 on 2/4/2016.
 */

var settings = {
    core: {
        PORT: 3000
    },
    db: {
        MONGO: {},
        NEO4J: {
            url: 'http://neo4j:admin@localhost:7474'
        },
        POSTGRES: {
            url: "postgres://postgres:admin@localhost:5433/TravellingDb"
        }
    },
    content: {
        imageFolderPath: __dirname + "\\public\\content\\images\\"
    },
    cookieSecret: "cookiesecurekey"
};

module.exports = settings;