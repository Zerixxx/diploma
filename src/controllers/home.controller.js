/**
 * Created by 1 on 3/25/2016.
 */


var express = require('express'),
    router = express.Router();

module.exports = function(){
    "use strict";

    router.get('/', function(req,res){
        if(req.session.user == null){
            res.redirect('/account/login');
        }else{
            res.redirect('/profile/' + req.session.user.id);
        }

    });

    router.get('/home', function(req,res){
        if(req.session.user == null){
            res.redirect('/account/login');
        }else{
            res.redirect('/profile/' + req.session.user.id);
        }
    });

    return router;
};