/**
 * Created by 1 on 2/4/2016.
 */
var express = require('express'),
    router = express.Router(),
    User = require('../models/User.model.js'),
    AuthProvider = require('../services/auth.service');
    logger = require('../utils/winston.util');

module.exports = function(){
    "use strict";

    //Login
    router.get('/login', function (req,res) {
        res.render('account/login.ejs');
    });

    router.post('/login', function(req,res){
        let loginModel = req.body.loginModel;

        let onSuccess = (user) => {
            // save only base user model into  session
            let url = '/profile/' + user.id;
            req.session.user = user;
            req.session.placesOnMap = [];
            req.session.placesInRoute = [];
            res.json({user: user, redirectUrl: url});
        };
        let onFailure = (err) => {
            res.status(400).json({status: 'error', msg: err});
        };

        AuthProvider.login(loginModel, onFailure, onSuccess);
    });

    //Register
    router.get('/register', function (req,res) {
        res.render('account/register.ejs');
    });

    router.post('/register', function (req,res) {
        let onSuccess = () => {
            res.json({returnUrl: '/account/login'});
        };
        let onFailure = (msg) => {
            res.status(400).json({status: "success", msg: msg});
        };

        AuthProvider.register(req.body.registrationModel,onFailure,onSuccess);
    });

    router.get('/logout', function(req,res){
        req.session.user = undefined;
        res.render('account/login.ejs');
    });


    return router;
};

