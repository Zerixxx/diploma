/**
 * Created by 1 on 3/25/2016.
 */


var express = require('express'),
    router = express.Router(),
    UserService = require('../models/User.model.js'),
    AddressService = require('../models/Address.model.js'),
    ImageService = require('../models/Image.model.js'),
    logger = require('../utils/winston.util'),
    multer = require('multer'),
    settings = require('../settings'),
    upload = multer({dest: settings.content.imageFolderPath}),
    authenticate = require('../middleware/session-auth');


module.exports = function(){
    "use strict";

    router.get('/places-on-map', function(req,res){
        res.json(req.session.placesOnMap);
    });

    router.post('/comments/add', function(req,res){
        let model = req.body.comment,
            userService = new UserService();

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        userService.addProfileComment(model, fail, function(){
            return res.json();
        });
    });

    router.get('/user-info', function(req,res){
        res.json(req.session.user);
    });

    router.get('/settings-page', function(req,res){
        res.render('profile/settings.ejs');
    });

    router.post('/settings',upload.array('images'), function(req,res){
        let model = {
            userId: req.session.user.id,
            email: req.body.user.email,
            phone: req.body.user.phone || ''
        },
            avatar = {
                name: req.files[0].filename,
                typeId: 1,
                description: '',
                ownerId: req.session.user.id
            };

        let imageService = new ImageService(),
            userService = new UserService();

        function fail(msg){
            return res.json(msg);
        }

        imageService.save(avatar, fail, function(images){
            if(images.length > 0){
                model.avatarImageId = images[0].id;
                userService.updateUserSettings(model, fail, function(){
                    return res.send();
                });
            }else{
                fail('Avatar is not saved');
            }
        });
    });

    router.get('/comments/:profileId', function(req,res){
        let userService = new UserService(),
            profileId = req.params.profileId;

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        userService.getProfileComments(profileId, fail, function(comments){
            return res.json(comments);
        });
    });

    router.get('/:id', function(req,res){
        let userId = req.params.id,
            userService = new UserService(),
            imageService = new ImageService();

        var fail = function(msg){
            res.render('error.ejs', {errorMsg: msg});
        };

        userService.findUserById(userId, fail, function(users){
            if(users.length === 1){
                if(users[0].iscompletelyregistered){
                    imageService.findUserAvatar(users[0].id, fail, function(images){
                        users[0].avatar = images.length > 0 ? images[0] : {};
                        res.render('profile/profile.ejs', {user: users[0]});
                    });
                }else{
                    res.render('profile/settings.ejs', {user: users[0]});
                }
            }else{
                fail('Something is wrong with users table.')
            }
        });

    });

    router.get('/', function(req,res){
        res.render('profile/profile.ejs', {user: req.session.user});
    });

    return router;
};
