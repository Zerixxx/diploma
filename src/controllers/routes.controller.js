/**
 * Created by Owner on 5/23/2016.
 */

/**
 * Created by 1 on 2/4/2016.
 */
var express = require('express'),
    router = express.Router(),
    User = require('../models/User.model.js'),
    AuthProvider = require('../services/auth.service'),
    RouteService = require('../models/Route.model'),
    PlaceService = require('../models/Place.model');
logger = require('../utils/winston.util');

module.exports = function(){
    "use strict";

    router.get('/', function(req,res){
        res.render('routes/route-page.ejs');
    });

    router.get('/:id/places', function(req,res){
        let model = {
            routeId: req.params.id,
            userId: req.session.user.id
        },
            routeService = new RouteService(),
            placeService = new PlaceService();

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        routeService.getPlaces(model, fail, function (places) {
            if(places.length > 0){
                for(let i = 0; i < places.length; i++){
                    placeService.findPlaceAvgRating(places[i].id, fail, function(rating){
                        places[i].avgrating = rating.length > 0 && rating[0].avgrating
                            ? Math.round( rating[0].avgrating * 10 ) / 10
                            : 0;
                    });
                    if(i == places.length - 1){
                        return res.json(places);
                    }
                }
            }else{
                return res.json(places);
            }
        });

        return res.json(places);
    });

    router.post('/add-place', function(req,res){
        let model = {
            placeId: req.body.placeId,
            routeId: req.body.routeId
        },
            routeService = new RouteService();

        function fail(msg){
            return res.status(400).json(msg);
        }

        routeService.addPlaceToRoute(model, fail, function(result){
            res.json();
        });
    });

    router.post('/remove-place', function(req,res){
        let model = {
                placeId: req.body.placeId,
                routeId: req.body.routeId
            },
            routeService = new RouteService();

        function fail(msg){
            return res.status(400).json(msg);
        }

        routeService.removePlaceFromRoute(model, fail, function(result){
            res.json();
        });
    });

    return router;
};

