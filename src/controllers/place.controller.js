/**
 * Created by 1 on 4/11/2016.
 */


var express = require('express'),
    router = express.Router(),
    logger = require('../utils/winston.util'),
    Place = require('../models/Place.model'),
    Image = require('../models/Image.model'),
    Address = require('../models/Address.model'),
    PlaceType = require('../models/PlaceType.model'),
    utils = require('../utils/utils'),
    multer = require('multer'),
    settings = require('../settings'),
    upload = multer({dest: settings.content.imageFolderPath});

module.exports = function(){
    "use strict";

    router.post('/save-to-map', function(req,res){
        let placeId = req.body.placeId,
            placeService = new Place();

        let fail = function(err){
            return res.status(400).json(err);
        };

        let exists = false;

        for(let i = 0; i < req.session.placesOnMap.length; i++){
            if(req.session.placesOnMap[i].id == placeId) {
                exists = true;
                break;
            }
        }

        if(!exists){
            placeService.findPlaceById(placeId,fail,function(places){
                if(places.length > 0){
                    places[0].ownerid = req.session.user.id;
                    places[0].startdate = utils.convertToDateString(places[0].startdate);
                    places[0].enddate = utils.convertToDateString(places[0].enddate);
                    places[0].permanent = places[0].startdate == places[0].enddate;
                    places[0].allday = places[0].openingtime == places[0].closingtime;
                    places[0].isfree = places[0].minprice == places[0].maxprice;
                    places[0].fulladdress = utils.buildFullAddress(places[0].country, places[0].region, places[0].city, places[0].street, places[0].building);

                    placeService.findPlaceAvgRating(placeId, fail, function(rating){
                        places[0].avgrating = rating.length > 0 ? rating[0].avgrating : 0;
                        req.session.placesOnMap.push(places[0]);
                        return res.json();
                    });

                }else{
                    fail('place not exists | count of places: ' + places.length);
                }
            });
        }else{
            fail('exists');
        }
    });

    router.post('/save-to-route', function(req,res){
        let placeId = req.body.placeId,
            placeService = new Place();

        let fail = function(err){
            return res.status(400).json(err);
        };

        let exists = false;

        for(let i = 0; i < req.session.placesInRoute.length; i++){
            if(req.session.placesInRoute[i].id == placeId) {
                exists = true;
                break;
            }
        }

        if(!exists){
            placeService.findPlaceById(placeId,fail,function(places){
                if(places.length > 0){
                    places[0].ownerid = req.session.user.id;
                    places[0].startdate = utils.convertToDateString(places[0].startdate);
                    places[0].enddate = utils.convertToDateString(places[0].enddate);
                    places[0].permanent = places[0].startdate == places[0].enddate;
                    places[0].allday = places[0].openingtime == places[0].closingtime;
                    places[0].isfree = places[0].minprice == places[0].maxprice;
                    places[0].fulladdress = utils.buildFullAddress(places[0].country, places[0].region, places[0].city, places[0].street, places[0].building);

                    placeService.findPlaceAvgRating(placeId, fail, function(rating){
                        places[0].avgrating = rating.length > 0 ? rating[0].avgrating : 0;
                        req.session.placesInRoute.push(places[0]);
                        return res.json();
                    });
                }else{
                    fail('place not exists | count of places: ' + places.length);
                }
            });
        }else{
            fail('exists');
        }
    });

    router.post('/comments/add', function(req,res){
        let model = req.body.comment,
            placeService = new Place();

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        placeService.addPlaceComment(model, fail, function(){
            return res.json();
        });
    });

    router.get('/', function(req, res){
        res.render('travelling/travel-index.ejs')
    });

    router.get('/create-new', function(req,res){
        res.render('travelling/create-new.ejs');
    });

    router.get('/types', function (req,res) {
        let fail = function(){
            return res.status(500).send('Can not get place types');
        };
        let success = function(result){
            return res.json(result);
        };

        let placeType = new PlaceType();
        placeType.getAllPlaceTypes(fail,success);
    });

    router.post('/save',upload.array('images'), function(req,res){
        let place = req.body.place,
            avatar = {
                name: req.files[0].filename,
                typeId: 2,
                description: '',
                ownerId: req.session.user.id
            },
            address = req.body.address;
        
        let placeService = new Place(),
            imageService = new Image(),
            addressService = new Address();

        // assign user id to entities
        place.ownerId = req.session.user.id;

        let errors = placeService.validate(place);
        if(errors.length > 0){
            return res.status(400).send('Place model is not valid!');
        }

        errors = addressService.validate(address);
        if(errors.length > 0){
            return res.status(400).send('Address model is not valid!');
        }

        errors = imageService.validate(avatar);

        if(errors.length > 0){
            return res.status(400).send('Avatar model is not valid!');
        }

        function fail(msg){
            return res.status(400).send(msg);
        }

        // save 1 place, 2 or 1 images, 1 address
        addressService.save(address,fail,function(addresses){
            place.addressId = addresses[0].id;
            imageService.save(avatar,fail,function (image1){
                place.avatarImageId = image1[0].id;
                placeService.save(place,fail,function(place){
                    return res.send(place[0]);
                });
            });
        });
    });

    router.post('/rating/save', function (req,res) {
        let model = {
                rating: req.body.rating,
                placeId: req.body.placeId,
                userId: req.session.user.id
            },
            placeService = new Place();

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        placeService.getUserRatingForPlace(model.placeId, model.userId, fail, function(rating){
            if(rating.length > 0){
                placeService.updatePlaceRating(model, fail, function(){
                    return res.send();
                });
            }else{
                placeService.insertPlaceRating(model, fail, function(){
                    return res.send();
                });
            }
        });
    });

    router.post('/search', function(req,res){
        let searchModel = {
            name: req.body.name,
            type: req.body.type,
            country: req.body.address.country,
            region: req.body.address.region,
            city: req.body.address.city
        };

        console.log(searchModel);

        let placeService = new Place();

        let fail = function(msg){
            return res.status(400).json(msg);
        };
        
        placeService.searchPlaces(searchModel, function (error) {
            res.status(400).json(error);
        }, function(places){
            console.log(places);
            for(let i = 0; i < places.length; i++){
                placeService.findPlaceAvgRating(places[i].id, fail, function(rating){
                    places[i].avgrating = rating.length > 0 && rating[0].avgrating
                        ? rating[0].avgrating
                        : 0;
                    placeService.getUserRatingForPlace(places[i].id, req.session.user.id, fail, function(userRating){
                        places[i].userrating = userRating.length > 0 && userRating[0].userrating
                            ? userRating[0].userrating
                            : 0;
                        if(i == places.length - 1){
                            res.json(places);
                        }
                    });
                })
            }
        });
    });

    router.get('/comments/:placeId', function(req,res){
        let placeService = new Place(),
            placeId = parseInt(req.params.placeId);

        let fail = function(msg){
            return res.status(400).json(msg);
        };

        placeService.getPlaceComments(placeId, fail, function(comments){
            return res.json(comments);
        });
    });

    router.get('/:placeId/avg-rating', function(req,res){
        let placeId = req.params.placeId,
            userId = req.session.user.id,
            placeService = new Place();

        placeService.getUserRatingForPlace(placeId, userId, function(msg){
            return res.status(400).json(msg);
        }, function(rating){
            let value = rating.length > 0 && rating[0].userrating
                ? rating[0].userrating
                : 0;
            res.json(value);
        })
    });

    router.get('/:id', function(req,res){
        let placeId = parseInt(req.params.id),
            placeService = new Place(),
            imageService = new Image();

        let fail = function(err){
            logger.error(err);
            return res.render('travelling/place-not-found.ejs', {error: err});
        };

        placeService.findPlaceById(placeId,fail,function(places){
            if(places.length > 0){
                places[0].ownerid = req.session.user.id;
                places[0].startdate = utils.convertToDateString(places[0].startdate);
                places[0].enddate = utils.convertToDateString(places[0].enddate);
                places[0].permanent = places[0].startdate == places[0].enddate;
                places[0].allday = places[0].openingtime == places[0].closingtime;
                places[0].isfree = places[0].minprice == places[0].maxprice;
                places[0].fulladdress = utils.buildFullAddress(places[0].country, places[0].region, places[0].city, places[0].street, places[0].building);

                placeService.findPlaceAvgRating(placeId, fail, function(rating){
                    places[0].avgrating = rating.length > 0 ? rating[0].avgrating : 0;
                    return res.render('travelling/place.ejs',{
                        place: places[0]
                    });
                });

            }else{
                fail('place not exists | count of places: ' + places.length);
            }
        });
    });


    


    return router;
};


