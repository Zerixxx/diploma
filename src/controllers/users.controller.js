/**
 * Created by 1 on 6/15/2016.
 */

var express = require('express'),
    router = express.Router(),
    UserService = require('../models/User.model.js'),
    ImageService = require('../models/Image.model.js'),
    AuthProvider = require('../services/auth.service'),
    RouteService = require('../models/Route.model'),
    logger = require('../utils/winston.util');

module.exports = function(){
    "use strict";

    router.get('/search-page', function (req,res) {
        res.render('users/search.ejs');
    });

    router.get('/search', function(req,res){
        let model = {
            profileId: parseInt(req.query.profileId) || 0,
            userName: req.query.userName
        },
            userService = new UserService(),
            imageService = new ImageService();

        function fail(msg){
            return res.status(400).json(msg);
        }

        userService.searchForUsers(model, fail, function(users){
            for(let i = 0; i < users.length; i++){
                imageService.findUserAvatar(users[i].id, fail, function (avatars) {
                    if(avatars.length > 0){
                        users[i].avatar = avatars[0];
                    }
                    if(i == users.length - 1){
                        return res.json(users);
                    }
                });
            }

        });
    });



    return router;
};

