/**
 * Created by 1 on 3/22/2016.
 */

"use strict";

let User = require('../models/User.model.js'),
    ValidationProvider = require('../models/validation.provider.js'),
    logger = require('../utils/winston.util'),
    neoHelper = require('../utils/neoHelper'),
    bcrypt = require('bcrypt-nodejs');

class AuthService {
    static login(loginModel,fail,success){
        let isValid = ValidationProvider.validateLoginModel();
        let user = new User();

        if(!isValid(loginModel)){
            logger.error('invalid login model %s', JSON.stringify(loginModel));
            fail('invalid login model');
        }else{
            user.findUserByEmail(loginModel.email,
                // fail
            function(err){
                logger.error(err);
                throw err;
            },
                //success
            function(result){
                if(result.length > 0){
                    if(bcrypt.compareSync(loginModel.password, result[0].password)){
                        success(result[0]);
                    }else{
                        fail('Password is incorrect!');
                    }
                }else{
                    fail('Input credentials are incorrect!');
                }
            });
        }
    }

    static register(registrationModel, fail, success){
        let isValid = ValidationProvider.validateRegistrationModel(),
            user = new User();

        if(!isValid(registrationModel)){
            logger.error('invalid registration model %s', JSON.stringify(registrationModel));
            fail('invalid registration model');
        }else{
            registrationModel.password = bcrypt.hashSync(registrationModel.password);
            user.save(registrationModel,fail,success);
        }
    }
}

module.exports = AuthService;
