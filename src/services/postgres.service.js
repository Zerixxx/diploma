/**
 * Created by Owner on 5/21/2016.
 */

"use strict";

let pg = require('pg'),
    ValidationProvider = require('../models/validation.provider.js'),
    logger = require('../utils/winston.util'),
    conString = require('../settings').db.POSTGRES.url;

class PgService {

    // query example = "INSERT INTO items(text, complete) values($1, $2)"
    // params example [$1, $2]
    static execute(query, params, fail, success){
        pg.connect(conString, function(err, client, done) {
            let results = [];
            if(err) {
                fail(err);
                return console.error('error fetching client from pool', err);
            }

            let q = client.query(query, params);

            q.on('error', function(error){
                done();
                fail(error);
                return console.error('error running query', error);
            });

            q.on('row', function(row){
                results.push(row);
            });

            q.on('end', function(){
                done();
                success(results);
            });
        });
    }
}

module.exports = PgService;

