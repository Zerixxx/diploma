/**
 * Created by Owner on 5/22/2016.
 */

/**
 * Created by owner on 20.05.16.
 */

"use strict";

var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidationProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query');

class PlaceTypeService{

    validate(model){
        return ValidationProvider.validatePlaceTypeModel()(model);
    }

    findPlaceType(name,fail,success){
        if(name != null){
            db.execute(queries.findPlaceTypeQuery,[name],fail,success);
        }else{
            fail('PlaceType is not specified!');
        }
    }

    findPlaceTypeById(typeId,fail,success){
        if(typeId != null){
            db.execute(queries.findPlaceTypeByIdQuery,[typeId],fail,success);
        }else{
            fail('PlaceType id is not specified!');
        }
    }
    
    getAllPlaceTypes(fail,success){
        db.execute(queries.getAllPlaceTypesQuery,null,fail,success);
    }

    save(name,fail,success){
        if(!name){
            fail('placeType name is not defined');
            return;
        }

        let ifPlaceTypeNotExists = function(res){
            if(res.length == 0){
                db.execute(queries.createPlaceQuery,[name],fail,success);
            }
        };
        db.execute(queries.findPlaceTypeQuery,[name],fail,ifPlaceTypeNotExists);
    }
}

module.exports = PlaceTypeService;
