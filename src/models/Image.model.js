/**
 * Created by Owner on 5/22/2016.
 */

/**
 * Created by owner on 20.05.16.
 */

"use strict";

var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidationProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query'),
    pgHelper = require('../utils/pgHelper'),
    fs = require('fs'),
    settings = require('../settings');

class ImageService{

    validate(model){
        return ValidationProvider.validateImageModel()(model);
    }

    generateUniqueImageName(imageName){
        imageName = imageName.split('\\');
        imageName = imageName[imageName.length-1];
        if(typeof (imageName) == 'string'){
            let parts = imageName.split('.');
            parts[0] += (Math.floor(Math.random() * 1000000000)).toString();
            return parts.join('.');
        }
        return null;
    }

    findImagesByPlaceId(placeId,fail,success){
        if(placeId != null){
            db.execute(queries.findImagesByPlaceIdQuery,[placeId],fail,success);
        }else{
            fail('Place Id in findImageByPlaceId not specified!');
        }
    }

    findImage(name,fail,success){
        if(typeof (name) == 'string'){
            db.execute(queries.findImageQuery,[name],fail,success);
        }else{
            fail('Image not found!');
        }
    }

    findUserAvatar(userId, fail, success){
        if(userId != null){
            db.execute(queries.getUserAvatarQuery,[userId],fail,success);
        }else{
            fail('User avatar is not found!');
        }
    }

    // pass place and userEmail model
    save(image,fail,success){
        if(!image){
            fail('Image is not defined');
            return;
        }

        let searchModel = [image.name];
        let createModel = [image.name, image.description, image.typeId, image.ownerId];

        db.execute(queries.createImageQuery,createModel,fail,function(){
            db.execute(queries.findImageQuery,searchModel,fail,function(result){
                success(result);
            });
        });
    }
}

module.exports = ImageService;
