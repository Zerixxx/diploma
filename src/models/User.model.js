/**
 * Created by owner on 20.02.16.
 */

"use strict";
var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidatorProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query'),
    pgHelper = require('../utils/pgHelper');


class UserService{

    validate(model){
        return ValidatorProvider.validateUserModel()(model);
    }

    findUserByEmail(email, fail, success){
        if(email != null){
            db.execute(queries.findUserByEmailQuery,[email],fail,success);
        }else{
            fail('User email is not specified!');
        }
    }

    findUserById(userId, fail, success){
        if(userId != null){
            db.execute(queries.findUserByIdQuery,[userId],fail,success);
        }else{
            fail('User id is not specified!');
        }
    }

    searchForUsers(model, fail, success){
        if(model != null){
            db.execute(queries.searchForUsersQuery,[model.profileId, '%' + model.userName + '%'], fail, success);
        }else{
            fail('User search model is not specified!');
        }
    }

    updateUserSettings(model, fail, success){
        if(model != null){
            db.execute(queries.updateUserSettingsQuery,[model.email, model.phone, model.userId], fail, success);
        }else{
            fail('User search model is not specified!');
        }
    }

    getProfileComments(profileId, fail, success){
        if(profileId != null){
            db.execute(queries.getProfileCommentsQuery,[profileId], fail, success);
        }else{
            fail('Cannot found profile comments!');
        }
    }

    addProfileComment(model, fail, success){
        if(model != null){
            db.execute(queries.addProfileCommentQuery,[model.commentText, model.creationDate, model.ownerId, model.receiverId], fail, success);
        }else{
            fail('Cannot found profile comments!');
        }
    }

    
    save(user,fail,success){
        if(!user){
            fail('address data is not defined');
            return;
        }

        let model = pgHelper.getParams(user);
        let ifNotExists = function(res){
            if(res.length == 0){
                db.execute(queries.createUserQuery,model,fail,success);
            }else{
                fail('User with such credentials already exists.')
            }
        };
        db.execute(queries.findUserByEmailQuery,[user.email],fail,ifNotExists);
    }
}

module.exports = UserService;







