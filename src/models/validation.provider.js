/**
 * Created by 1 on 3/21/2016.
 */

"use strict";

let Validator = require('jsonschema').Validator,
    validator = new Validator();

class ValidationProvider{

    static validateRouteModel(){
        let baseSchema = require('./schemas/Image.schema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }
    
    static validateImageModel(){
        let baseSchema = require('./schemas/Image.schema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }

    static validatePlaceTypeModel(){
        let baseSchema = require('./schemas/PlaceType.schema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }

    static validateUserModel(){
        let baseSchema = require('./schemas/.schema');
        validator.addSchema(baseSchema, '/AddressSchema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }

    static validateAddressModel(){
        let baseSchema = require('./schemas/Address.schema');
        validator.addSchema(baseSchema, '/AddressSchema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }

    static validatePlaceModel(){
        let baseSchema = require('./schemas/Place.schema'),
            addressSchema = require('./schemas/Address.schema');
        validator.addSchema(addressSchema, '/AddressSchema');

        return (instance) => {
            return validator.validate(instance,baseSchema).errors;
        }
    }

    static validateLoginModel(){
        let schema = require('./schemas/Login.schema');

        return (instance) => {
            return validator.validate(instance,schema).errors;
        }
    }

    static validateRegistrationModel(){
        let schema = require('./schemas/Registration.schema');

        return (instance) => {
            return validator.validate(instance,schema).errors;
        }
    }
}

module.exports = ValidationProvider;