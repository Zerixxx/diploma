/**
 * Created by Owner on 5/21/2016.
 */

"use strict";
var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidationProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query');

class AddressService{

    validate(model){
        return ValidationProvider.validateAddressModel()(model);
    }

    findAddress(address, fail, success){
        let model = [address.country, address.region, address.city];
        db.execute(queries.findAddressQuery,model,fail,success);
    }

    findAddressOfPlace(placeId, fail, success){
        if(placeId != null){
            db.execute(queries.findAddressOfPlaceQuery,[placeId],fail,success);
        }else{
            fail('Place Id is not specified in findAddressOfPlaceQuery!');
        }
    }
    
    save(address,fail,success){
        if(!address){
            fail('address data is not defined');
            return;
        }
        let model = [address.country,address.region,address.city];
        db.execute(queries.findAddressQuery,model,fail,function(result){
            if(result.length == 0){
                db.execute(queries.createAddressQuery,model,fail,function(){
                    db.execute(queries.findAddressQuery,model,fail,function(result){
                        success(result);
                    });
                });
            }else{
                success(result);
            }
        });
    }
}

module.exports = AddressService;








