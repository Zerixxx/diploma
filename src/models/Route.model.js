/**
 * Created by owner on 20.02.16.
 */

"use strict";
var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidatorProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query'),
    pgHelper = require('../utils/pgHelper');


class RouteService{

    validate(model){
        //return ValidatorProvider.validateUserModel()(model);
    }

    addPlaceToRoute(model, fail, success){
        if(model != null){
            db.execute(queries.addPlaceToRouteQuery,[model.routeId, model.placeId],fail,success);
        }else{
            fail('Route model is not specified!');
        }
    }

    removePlaceFromRoute(model, fail, success){
        if(model != null){
            db.execute(queries.removePlaceFromRouteQuery,[model.routeId, model.placeId],fail,success);
        }else{
            fail('Route model is not specified!');
        }
    }
    
    getPlaces(model, fail, success){
        if(model != null){
            db.execute(queries.getRoutePlacesQuery,[model.routeId, model.userId],fail,success);
        }else{
            fail('Route model is not specified!');
        }
    }
    
    createRoute(model, fail, success){
        if(model != null){
            db.execute(queries.createRouteQuery,[model.name, model.creationDate, model.userId],fail,success);
        }else{
            fail('Route model is not specified!');
        }
    }
}

module.exports = RouteService;







