/**
 * Created by owner on 20.05.16.
 */

"use strict";

var settings = require('../settings'),
    db = require('../services/postgres.service'),
    ValidationProvider = require('./validation.provider.js'),
    logger = require('../utils/winston.util'),
    queries = require('./queries/query'),
    pgHelper = require('../utils/pgHelper'),
    utils = require('../utils/utils');

class PlaceService{

    validate(model){
        return ValidationProvider.validatePlaceModel()(model);
    }

    findPlace(place,userEmail,fail,success){
        if(place != null){
            let model = [place.name, place.type,userEmail];
            db.execute(queries.findPlaceQuery,model,fail,success);
        }else{
            fail('Place is not specified!');
        }
    }

    findPlaceById(id,fail,success){
        if(id != null){
            db.execute(queries.findPlaceByIdQuery,[id],fail,success);
        }else{
            fail('Place Id is not specified!');
        }
    }

    searchPlaces(searchModel, fail, success){
        if(searchModel != null){
            let params = ['%' + searchModel.name + '%',
                '%' + searchModel.type + '%',
                '%' + searchModel.country + '%',
                '%' + searchModel.region + '%',
                '%' + searchModel.city + '%'];
            db.execute(queries.searchPlacesQuery, params, fail, success);
        }else{
            fail('search model is not defined');
        }
    }

    findPlaceAvgRating(placeId, fail, success){
        if(placeId != null){
            db.execute(queries.findPlaceRatingQuery, [placeId], fail, success);
        }else{
            fail('cannot get place rating');
        }
    }
    
    getUserRatingForPlace(placeId, userId, fail, success){
        if(placeId != null && userId != null){
            db.execute(queries.getUserRatingForPlaceQuery, [placeId, userId], fail, success);
        }else{
            fail('cannot get user rating for place');
        }
    }

    insertPlaceRating(model, fail, success){
        if(model != null){
            db.execute(queries.insertRatingQuery, [model.rating, model.placeId, model.userId], fail, success);
        }else{
            fail('rating insert model is not correct');
        }
    }



    updatePlaceRating(model, fail, success){
        if(model != null){
            db.execute(queries.updateRatingQuery, [model.rating, model.placeId, model.userId], fail, success);
        }else{
            fail('rating model is null');
        }
    }

    getPlaceComments(placeId, fail, success){
        if(placeId != null){
            db.execute(queries.getPlaceCommentsQuery, [placeId], fail, success);
        }else{
            fail('rating model is null');
        }
    }

    addPlaceComment(model, fail, success){
        if(model != null){
            db.execute(queries.addPlaceCommentQuery, [model.commentText, model.creationDate, model.ownerId, model.placeId], fail, success);
        }else{
            fail('rating model is null');
        }
    }

    // pass place and userEmail model
    save(place,fail,success){
        if(!place){
            fail('address data is not defined');
            return;
        }
        let searchModel = [place.name,place.typeId,place.ownerId];
        let createModel = [place.name, place.startDate, place.endDate, place.openingTime, place.closingTime,
            place.minPrice, place.maxPrice, place.street, place.building, place.latitude, place.longitude,
            place.phone, place.email, place.shortDescription, place.fullDescription, place.addressId, place.avatarImageId, place.typeId,
            place.ownerId];

        db.execute(queries.findPlaceQuery,searchModel,fail,function (results) {
            if(results.length == 0){
                db.execute(queries.createPlaceQuery,createModel,fail,function(){
                    db.execute(queries.findPlaceQuery,searchModel,fail,function (results) {
                        success(results);
                    });
                });
            }else{
                success(results);
            }
        });
    }
}

module.exports = PlaceService;
