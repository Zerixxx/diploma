/**
 * Created by Owner on 5/21/2016.
 */

var queries = {
    // place
    createPlaceQuery: "INSERT INTO Places (name, startDate, endDate, openingTime, closingTime, minPrice, maxPrice, street, building, latitude, longitude, phone, email, shortDescription, fullDescription, addressId, avatarImageId, typeId, ownerId) " +
    "VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)",
    findPlaceQuery: "SELECT " +
    "p.id, p.name," +
    "p.startDate,p.endDate," +
    "p.openingTime,p.closingTime," +
    "p.minPrice,p.maxPrice," +
    "p.street,p.building," +
    "p.latitude,p.longitude," +
    "p.phone,p.email," +
    "p.shortDescription,p.fullDescription," +
    "p.avatarImageId, p.addressId, p.ownerId, " +
    "a.country,a.region,a.city,t.id as typeid,t.name as typename," +
    "im.name as avatarimagename " +
    "FROM Places as p " +
    "JOIN Addresses as a ON a.id = p.addressId " +
    "JOIN PlaceTypes as t ON p.typeId = t.id " +
    "JOIN Images as im ON p.avatarImageid = im.id " +
    "WHERE p.name = ($1) AND p.typeId = ($2) AND p.ownerId = ($3)",

    findPlaceByIdQuery:  "SELECT " +
    "p.id, p.name," +
    "p.startDate,p.endDate ," +
    "p.openingTime ,p.closingTime ," +
    "p.minPrice ,p.maxPrice ," +
    "p.street,p.building," +
    "p.latitude,p.longitude," +
    "p.phone,p.email," +
    "p.shortDescription,p.fullDescription," +
    "p.avatarImageId, p.addressId , p.ownerId , " +
    "a.country,a.region,a.city,t.id as typeId,t.name as typename, im.name as avatarimagename " +
    "FROM Places as p " +
    "JOIN Addresses as a ON a.id = p.addressId " +
    "JOIN PlaceTypes as t ON p.typeId = t.id " +
    "JOIN Images as im ON p.avatarImageId = im.id " +
    "WHERE p.id = ($1);",

    searchPlacesQuery: "SELECT " +
    "p.id, p.name," +
    "p.startDate,p.endDate," +
    "p.openingTime,p.closingTime," +
    "p.minPrice,p.maxPrice," +
    "p.street,p.building," +
    "p.latitude,p.longitude," +
    "p.phone,p.email," +
    "p.shortDescription,p.fullDescription," +
    "p.avatarImageId,p.addressId,p.ownerId" +
    ",a.country,a.region,a.city,t.id as typeId,t.name as typename, im.name as avatarimagename " +
    "FROM Places as p " +
    "JOIN Addresses as a ON p.addressId = a.id " +
    "JOIN PlaceTypes as t ON p.typeId = t.id " +
    "JOIN Images as im ON p.avatarImageId = im.id " +
    "WHERE p.endDate >= now() " +
    "AND p.name ILIKE ($1) " +
    "AND t.name ILIKE ($2) " +
    "AND ( a.country ILIKE ($3) " +
    "OR a.region ILIKE ($4) " +
    "OR a.city ILIKE ($5)) " +
    "ORDER BY p.endDate asc;",


    // user
    findUserByEmailQuery: "SELECT * FROM Users WHERE email = ($1);",
    findUserByIdQuery: "SELECT * FROM Users WHERE id = ($1);",
    getUserAvatarQuery: "SELECT im.name, im.id, im.typeId, imt.name as typeName FROM Images as im " +
    "JOIN ImageTypes as imt ON imt.id = im.typeId " +
    "JOIN Users as u ON u.id = im.ownerId " +
    "WHERE imt.id = '1' AND u.id = ($1)",
    getUserRolesQuery: "SELECT r.id as id, r.name as name " +
    "FROM UsersInRoles as ur " +
    "JOIN Roles as r ON ur.roleId = r.id " +
    "JOIN Users as u ON ur.userId = u.id " +
    "WHERE u.id = ($1);",
    createUserQuery: "INSERT INTO Users (firstName,lastName,email,password,isBlocked,addressId) VALUES ($1,$2,$3,$4,$5,$6)",
    searchForUsersQuery: "SELECT * FROM Users " +
    "WHERE id = ($1) OR LOWER(firstName || ' ' || lastName) ILIKE ($2);",
    updateUserSettingsQuery: "UPDATE Users SET email = ($1), phone = ($2), isCompletelyRegistered = 'true' WHERE id = ($3);",
    updateUserProfileAvatarQuery: "UPDATE Images as im " +
    "JOIN ImageTypes as imt ON imt.id = im.typeId " +
    "JOIN Users as u ON u.id = im.ownerId " +
    "WHERE imt.id = '1' AND u.id = ($1)" +
    "SET im.name = ($2);",

    // comments
    getProfileCommentsQuery: "SELECT uc.id, uc.commentText, uc.creationDate, uc.ownerId, uc.receiverId, rec.firstName as receivername, ow.firstName as ownername " +
    "FROM UserComments as uc " +
    "JOIN Users as ow ON ow.id = uc.ownerId " +
    "JOIN Users as rec ON rec.id = uc.receiverId " +
    "WHERE receiverId = ($1)",
    getPlaceCommentsQuery: "SELECT uc.id, uc.commentText, uc.creationDate, uc.ownerId, uc.placeId, ow.firstName as ownername " +
    "FROM UserComments as uc " +
    "JOIN Users as ow ON ow.id = uc.ownerId " +
    "JOIN Places as p ON p.id = uc.placeId " +
    "WHERE uc.placeId = ($1)",
    addProfileCommentQuery: "INSERT INTO UserComments (commentText, creationDate, ownerId, receiverId) VALUES ($1, $2, $3, $4);",
    addPlaceCommentQuery: "INSERT INTO UserComments (commentText, creationDate, ownerId, placeId) VALUES ($1, $2, $3, $4);",

    // address
    findAddressQuery: "SELECT * FROM Addresses WHERE country = ($1) AND region = ($2) AND city = ($3)",
    createAddressQuery: "INSERT INTO Addresses (country, region, city) VALUES ($1,$2,$3)",
    findAddressOfPlaceQuery: "SELECT * FROM Addresses AS a INNER JOIN Places AS p ON a.id = p.addressId WHERE p.id = ($1)",

    // place types
    findPlaceTypeQuery: "SELECT * FROM PlaceTypes WHERE name = ($1)",
    getAllPlaceTypesQuery: "SELECT * FROM PlaceTypes",
    findPlaceTypeByIdQuery: "SELECT * FROM PlaceTypes WHERE id = ($1)",

    // images
    findImageQuery: "SELECT * FROM Images WHERE name = ($1)",
    createImageQuery: "INSERT INTO Images (name,description,typeId, ownerId) VALUES ($1,$2,$3,$4)",
    findImagesByPlaceIdQuery:   "SELECT im.id, im.name, t.name as typename, p.id as placeid " +
                                "FROM Images AS im " +
                                "JOIN Places AS p ON p.avatarImageId = im.id " +
                                "JOIN ImageTypes AS t ON t.id = im.typeId " +
                                "WHERE p.id = ($1)",

    // routes
    findRouteByNameQuery: "SELECT * FROM Routes WHERE name = ($1) AND ownerId = ($2);",
    createRouteQuery: "INSERT INTO Routes (name,creationDate,ownerId) VALUES ($1,$2,$3);",
    addPlaceToRouteQuery: "INSERT INTO PlacesInRoutes (routeId, placeId) VALUES ($1, $2);",
    removePlaceFromRouteQuery: "DELETE FROM PlacesInRoutes WHERE routeId = ($1) AND placeId = ($2);",
    getRoutePlacesQuery: "SELECT " +
    "r.id as routeId, p.id as placeId, p.name," +
    "p.startDate,p.endDate," +
    "p.openingTime,p.closingTime," +
    "p.minPrice,p.maxPrice," +
    "p.street,p.building," +
    "p.latitude,p.longitude," +
    "p.phone,p.email," +
    "p.shortDescription,p.fullDescription," +
    "p.avatarImageId,p.addressId,p.ownerId, im.name as avatarName " +
    "FROM Routes as r " +
    "JOIN PlacesInRoutes as pr ON pr.routeId = r.id " +
    "JOIN Places as p ON p.id = pr.placeId " +
    "JOIN Images as im ON p.avatarImageId = im.id " +
    "WHERE r.id = ($1) AND r.ownerId = ($2);",

    //ratings
    findPlaceRatingQuery: "SELECT AVG(rating) as avgrating FROM PlaceRatings WHERE placeId = ($1);",
    getUserRatingForPlaceQuery: "SELECT rating as userrating FROM PlaceRatings WHERE placeId = ($1) AND userId = ($2);",
    updateRatingQuery: "UPDATE PlaceRatings SET rating = ($1) WHERE placeId = ($2) AND userId = ($3);",
    insertRatingQuery: "INSERT INTO PlaceRatings (rating, placeId, userId)  VALUES ($1,$2,$3);"

};

module.exports = queries;