/**
 * Created by Owner on 5/23/2016.
 */


var routeSchema = {
    "id": "/RouteSchema",
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "createDate": {"type": "string"},
        "ownerId": {"type": "string"}
    }
    ,
    "required": [
        "name",
        "createDate",
        "ownerId"]
};

module.exports = routeSchema;
