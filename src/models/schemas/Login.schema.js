/**
 * Created by 1 on 3/22/2016.
 */

var loginSchema = {
    "id": "/LoginSchema",
    "type": "object",
    "properties": {
        "email": { "type": "string"},
        "password": {"type": "string"}
    },
     "required": [
     "email",
     "password"]
};

module.exports = loginSchema;