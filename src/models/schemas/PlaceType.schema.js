/**
 * Created by Owner on 5/22/2016.
 */

var placeTypeSchema = {
    "id": "/PlaceTypeSchema",
    "type": "object",
    "properties": {
        "id": {"type": "string"},
        "name": { "type": "string"}
    },
    "required": [
        "id",
        "name"
    ]
};

module.exports = placeTypeSchema;

