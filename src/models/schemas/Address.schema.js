/**
 * Created by 1 on 3/18/2016.
 */

var addressSchema = {
    "id": "/AddressSchema",
    "type": "object",
    "properties": {
        "country": { "type": "string"},
        "region": { "type": "string"},
        "city": { "type": "string"}
    },
    "required": [
        "country",
        "city"
    ]
};

module.exports = addressSchema;
