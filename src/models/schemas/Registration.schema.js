/**
 * Created by 1 on 3/22/2016.
 */

var registrationSchema = {
    "id": "/RegisterSchema",
    "type": "object",
    "properties": {
        "firstName": {"type": "string"},
        "lastName": {"type": "string"},
        "email": { "type": "string"},
        "password": {"type": "string"}
    }
    ,
     "required": [
     "firstName",
     "lastName",
     "email",
     "password"]
};

module.exports = registrationSchema;