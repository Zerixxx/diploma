/**
 * Created by Owner on 5/22/2016.
 */


var imageSchema = {
    "id": "/ImageSchema",
    "type": "object",
    "properties": {
        "id" : { "type": "number"},
        "name": { "type": "string"},
        "description": { "type": "string"},
        "typeId": {"type": "number"},
        "ownerId": {"type": "number"}
    },
    "required": [
        "name",
        "typeId",
        "ownerId"]
};

module.exports = imageSchema;
