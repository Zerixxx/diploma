/**
 * Created by owner on 19.05.16.
 */

var placeSchema = {
    "id": "/PlaceSchema",
    "type": "object",
    "properties": {
        "name": { "type": "string"},
        "street": {"type": "string"},
        "building": {"type": "string"},
        "latitude": {"type": "string"},
        "longitude": {"type": "string"},
        "startDate": {"type": "string"},
        "endDate": {"type": "string"},
        "openingTime": {"type": "string"},
        "closingTime": {"type": "string"},
        "minPrice": {"type": "string"},
        "maxPrice": {"type": "string"},
        "phone": {"type": "string"},
        "email": {"type": "string"},
        "shortDescription": {"type": "string"},
        "fullDescription": {"type": "string"},/*,
        "email": {"type": "string"},/*,
        "addressId": {"type": "number"},
        "avatarImageId": {"type": "number"},
        "backgroundImageId": {"type": "number"},*/
        "typeId": {"type": "string"}/*,
        "ownerId": {"type": "number"}*/
    },
    "required": [
        "name",
        "latitude",
        "longitude",
        "typeId",
        "shortDescription",
        "fullDescription"/*,
        "avatarImageId",
        "addressId",
        "backgroundImageId",
        "ownerId"*/
    ]
};

module.exports = placeSchema;
