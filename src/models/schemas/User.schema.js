/**
 * Created by 1 on 3/18/2016.
 */

var userSchema = {
    "id": "/UserSchema",
    "type": "object",
    "properties": {
        "firstName": {"type": "string"},
        "lastName": {"type": "string"},
        "email": { "type": "string"},
        "password": {"type": "string"},
        "isBlocked": {"type": "boolean"},
        "phone": {"type": "string"},
        "birthDate": {"type": "date"}
    }
    /*,
    "required": [
        "firstName",
        "lastName"]*/
};

module.exports = userSchema;