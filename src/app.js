/**
 * Created by 1 on 2/4/2016.
 */

'use strict';

var express = require('express'),
    app = module.exports = express(),
    layout = require('express-ejs-layouts'),
    settings = require('./settings'),
    passport = require('passport'),
    flash = require('connect-flash'),
    bodyParser = require('body-parser'),
    logger = require('./utils/winston.util'),
    pg = require('pg'),
    session = require('express-session'),
    pgSession = require('connect-pg-simple')(session);

// Middleware
var authenticate = require('./middleware/session-auth');

app.engine('ejs', require('ejs').__express);
app.set('view engine', 'ejs');
app.set('layout','_layout');
app.set('view cache', false);

app.use(express.static(__dirname + '/public'));
app.use(layout);

//dev
//app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

//session set-up
app.set('trust proxy', 1);
app.use(session({
    store: new pgSession({
        pg : pg,                                  // Use global pg-module
        conString : settings.db.POSTGRES.url
    }),
    secret: settings.cookieSecret,
    resave: false,
    cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } // 30 days
}));

// set up local-passport auth
//require('./middleware/passport-local.config.js')(passport);


// set up controllers
app.use('/profile',authenticate,require('./controllers/user-profile.controller')());
app.use('/account',require('./controllers/account.controller')());
app.use('/test',require('./controllers/test.controller')());
app.use('/places',authenticate,require('./controllers/place.controller.js')());
app.use('/routes',authenticate,require('./controllers/routes.controller.js')());
app.use('/images',authenticate,require('./controllers/image.controller.js')());
app.use('/people',authenticate,require('./controllers/users.controller.js')());
app.use('/',require('./controllers/home.controller')());


app.listen(process.env.port || settings.core.PORT, function() {
    logger.info('App is started on port: %d', settings.core.PORT);
});
