/**
 * Created by Owner on 5/22/2016.
 */

"use strict";

let functions = {

    // object to array
    getParams: function(obj) {
        if(typeof (obj) !== "object")
            return null;
        let arr = [];
        for(let prop in obj){
            if(obj.hasOwnProperty(prop)){
                arr.push(obj[prop]);
            }
        }
        return arr;
    }
};

module.exports = functions;
