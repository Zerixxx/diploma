/**
 * Created by 1 on 3/21/2016.
 *
 * Settings for a winston logger
 */

var winston = require('winston');

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'app.log' })
    ]
});

module.exports = logger;
