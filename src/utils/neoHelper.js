/**
 * Created by 1 on 3/22/2016.
 */

"use strict";

let functions = {

    // returns first node data of passed label type
    firstNode: (results, label) => {
        if(results[0][label] == null)
            return null;

        //get main data
        let out = results[0][label]._data.data;

        // add id and label to result
        out.id = results[0][label]._data.metadata.id;
        out.label = results[0][label]._data.metadata.labels[0];
        return out;
    }
};


module.exports = functions;
