/**
 * Created by 1 on 5/30/2016.
 */


module.exports = {

    isNull: function(value){
        "use strict";
        return value == null || value == 'null' || value == '';
    },

    buildFullAddress: function (country,region,city,street,building){
        "use strict";
        let res = country;
        if(!this.isNull(region))  res += ', ' + region;
        res += ', ' + city;
        if(!this.isNull(street))  res += ',' + street;
        if(!this.isNull(building))    res += ',' + building;
        return res;
    },

    convertToDateString: function(pgDate){
        "use strict";
        if(pgDate != null){
            return pgDate.getFullYear() + '-' + (pgDate.getMonth()+1) + '-' + pgDate.getDate();
        }else{
            return null;
        }
    },

    convertToTimeString: function(pgDate){
        "use strict";
        if(pgDate != null){
            return pgDate.getHours() + ':' + pgDate.getMinutes();
        }else{
            return null;
        }
    }
};
