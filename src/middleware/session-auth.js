/**
 * Created by 1 on 3/22/2016.
 */

var session = require('express-session');

var auth = function(req,res,next){
    "use strict";
    let user = req.session.user;
    if(user == null){
        res.redirect('/account/login');
    }else{
        res.locals.user = req.session.user;
        next();
    }
};

module.exports = auth;




