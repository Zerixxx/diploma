CREATE DATABASE "TravellingDb";

CREATE TABLE "session" (
  "sid" varchar NOT NULL COLLATE "default",
	"sess" json NOT NULL,
	"expire" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "session" ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;

CREATE TABLE Addresses(
    id SERIAL PRIMARY KEY,
    country varchar(100) NOT NULL,
    region varchar(100) NULL,
    city varchar(100) NOT NULL
);

CREATE TABLE PlaceTypes(
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);

CREATE TABLE ImageTypes(
    id SERIAL PRIMARY KEY,
    name varchar(100) UNIQUE,
    minSize INT DEFAULT 0,
    maxSize INT Default 1000
);

CREATE TABLE Users (
    id SERIAL PRIMARY KEY,
    firstName varchar(50) NOT NULL,
    lastName varchar(50) NOT NULL,
    email varchar(100) NOT NULL,
    password text NOT NULL,
    phone varchar(20) NULL,
    isBlocked boolean DEFAULT false,
    isCompletelyRegistered boolean DEFAULT false,
    addressId INT references Addresses(id)
);

CREATE TABLE Images(
    id SERIAL PRIMARY KEY,
    name varchar(200) UNIQUE,
    description text,
    typeId INT references ImageTypes(id) NOT NULL,
    ownerId INT references Users(id) NOT NULL
);

CREATE TABLE UserRoles(
    id SERIAL PRIMARY KEY,
    roleName varchar(50) NOT NULL
);

CREATE TABLE UsersInRoles(
    id SERIAL PRIMARY KEY,
    userId INT references Users(id) NOT NULL,
    roleId INT references UserRoles(id) NOT NULL
);



CREATE TABLE ImageRatings(
    id SERIAL PRIMARY KEY,
    rating decimal CHECK(rating <= 10 AND rating >= 0) DEFAULT 0,
    userId INT references Users(id) NOT NULL,
    imageId INT references Images(id) NOT NULL
);

CREATE TABLE Places(
    id SERIAL PRIMARY KEY,
    name varchar(100) NOT NULL,
    startDate date,
    endDate date,
    openingTime time,
    closingTime time,
    minPrice double precision DEFAULT 0,
    maxPrice double precision DEFAULT 0 CHECK (maxPrice >= minPrice),
    street text,
    building varchar(20),
    latitude varchar(30) NOT NULL,
    longitude varchar(30) NOT NULL,
    phone varchar(20) NOT NULL,
    email varchar(50),
    shortDescription varchar(200) NOT NULL,
    fullDescription text NOT NULL,

    avatarImageId INT references Images(id) NOT NULL,
    addressId INT references Addresses(id) NOT NULL,
    typeId INT references PlaceTypes(id) NOT NULL,
    ownerId INT references Users(id) NOT NULL
);

CREATE TABLE Routes(
    id SERIAL PRIMARY KEY,
    name varchar(150),
    creationDate date,
    ownerId INT references Users(id) NOT NULL
);

CREATE TABLE PlacesInRoutes(
    id SERIAL PRIMARY KEY,
    routeId INT references Routes(id) NOT NULL,
    placeId INT references Places(id) NOT NULL
);

CREATE TABLE PlaceRatings(
    id SERIAL PRIMARY KEY,
    rating decimal CHECK(rating <= 10 AND rating >= 0) DEFAULT 0,
    placeId INT references Places(id) NOT NULL,
    userId INT references Users(id) NOT NULL
);

CREATE TABLE UserComments(
    id SERIAL PRIMARY KEY,
    commentText text NOT NULL,
    creationDate date NOT NULL,
    ownerId INT references Users(id) NOT NULL,
    receiverId INT references Users(id) NULL,
    placeId INT references Places(id) NULL,
    imageId INT references Images(id) NULL
);
